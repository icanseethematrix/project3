//
//  MenuViewController.h
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic, readwrite, strong) IBOutlet UIWebView *webView;

@end
