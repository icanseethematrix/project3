//
//  Menu.h
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Menu : NSObject

- (NSURLRequest *)getMenuWithName:(NSString *)name;

@end
