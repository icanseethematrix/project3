//
//  DiscussionViewController.h
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Parse/Parse.h>
#import <UIKit/UIKit.h>

@interface DiscussionViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, readwrite, strong) NSArray *discussions;
@property (nonatomic, readwrite, strong) IBOutlet UITableView *tblSimpleTable;

@end
