//
//  Calendar.h
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Calendar : NSObject

- (NSString *)getCalendarHTMLForEmail:(NSString *)email withHeight:(int)height;

@end
