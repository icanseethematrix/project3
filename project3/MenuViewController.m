//
//  MenuViewController.m
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "Menu.h"
#import "MenuViewController.h"

@implementation MenuViewController

@synthesize webView = _webView;


/*
 *  Loads the view.
 */

- (void)loadView
{
    [super loadView];
    
    // gets menu of designated type
    Menu *menu = [[Menu alloc] init];    
    [self.webView loadRequest:[menu getMenuWithName:self.navigationItem.title]];
}


@end
