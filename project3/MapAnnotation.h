//
//  MapAnnotation.h
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MapAnnotation : NSObject <MKAnnotation>

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString* subtitle;
@property (nonatomic, copy) NSString* title;
@property (nonatomic) NSURL* url;
@property (nonatomic) NSString* userData;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate title:(NSString*)title;

@end
