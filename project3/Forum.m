//
//  Forum.m
//  project3
//
//  Created by Joseph Botros on 4/30/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "Forum.h"

@implementation Forum


/*
 *  Gets desired type of data from Parse database.
 */

- (NSArray *)getPostsOfType:(NSString *)type withIdentifier:(NSString *)identifier
{
    NSMutableArray *posts = [[NSMutableArray alloc] init];
    PFQuery *query = [[PFQuery alloc] init];
     
    if (type == @"discussion")
    {
        // gets all discussions and orders alphabetically
        query = [PFQuery queryWithClassName:@"discussion"];
        [query whereKeyExists:@"discussion"];
        [query orderByAscending:@"discussion"];
    }
    
    else if (type == @"question")
    {
        // gets questions for given discussion and orders by most recent
        query = [PFQuery queryWithClassName:@"question"];
        [query whereKey:@"discussion" equalTo:identifier];
        [query orderByDescending:@"updatedAt"];        
    }

    else if (type == @"comment")
    {
        // gets single question containing comments of interest
        query = [PFQuery queryWithClassName:@"question"];
        [query whereKey:@"question" equalTo:identifier];        
    }
    
    // adds PFObjects to array for later use
    for (PFObject *object in [query findObjects])
        [posts addObject:object];
    
    return posts;
}


/*
 *  Saves a posted question or comment.
 */

- (void)savePost:(NSString *)post withName:(NSString *)name ofType:(NSString *)type andIdentifier:(NSString *)identifier
{
    if (type == @"comment")
    {
        // gets comments for given question
        PFQuery *query = [[PFQuery alloc] initWithClassName:@"question"];
        [query whereKey:@"question" equalTo:identifier];
        PFObject *question = [query getFirstObject];
        NSMutableArray *comments = [question objectForKey:@"comments"];

        // if no comments, create array and add this one
        if ([question objectForKey:@"comments"] == nil)
        {
            comments = [[NSMutableArray alloc] init];
            [comments addObject:post];
        }
        
        // else add comment to front of list (most recent first)
        else
        {
            comments = [question objectForKey:@"comments"];
            [comments insertObject:post atIndex:0];
        }

        // overwrite existing object with changes
        [question setObject:comments forKey:@"comments"];
        [question save];
    }
    
    else if (type == @"question")
    {
        // add question to database
        PFObject *question = [PFObject objectWithClassName:@"question"];
        [question setObject:post forKey:@"question"];
        [question setObject:name forKey:@"name"];
        [question setObject:identifier forKey:@"discussion"];
        [question save];
    }
}


@end
