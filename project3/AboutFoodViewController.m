//
//  AboutFoodViewController.m
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "AboutFoodViewController.h"

@implementation AboutFoodViewController

@synthesize scrollView = _scrollView;


/*
 *  Loads the view.
 */

- (void)loadView
{
    [super loadView];
    
    // make scroll view correct size
    self.scrollView.contentSize = CGSizeMake(320,720);    
}


@end
