//
//  MapAnnotation.m
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "MapAnnotation.h"

@implementation MapAnnotation

@synthesize coordinate = _coordinate;
@synthesize subtitle = _subtitle;
@synthesize title = _title;
@synthesize url = _url;
@synthesize userData = _userData;


/*
 *  Create annotation with the given properties.
 */

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate title:(NSString*)title;
{
    self.coordinate = coordinate;
    self.title = title;
    
    return self;
}


@end
