//
//  AboutHouseViewController.h
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <Parse/Parse.h>
#import <UIKit/UIKit.h>

@interface AboutHouseViewController : UIViewController <MKMapViewDelegate>

@property(nonatomic, strong) IBOutlet MKMapView *mapView;
@property(nonatomic, strong) IBOutlet UIScrollView *scrollView;

@end
