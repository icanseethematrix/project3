//
//  AboutFoodViewController.h
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutFoodViewController : UIViewController

@property(nonatomic, strong) IBOutlet UIScrollView *scrollView;

@end
