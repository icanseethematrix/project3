//
//  EventsViewController.m
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "EventsViewController.h"
#import "Calendar.h"

@implementation EventsViewController

@synthesize webView = _webView;


/*
 *  Loads the view.
 */

- (void)loadView
{
    [super loadView];
    
    // gets calendar for the house
    Calendar *calendar = [[Calendar alloc] init];
    NSString *html = [calendar getCalendarHTMLForEmail:@"dudleyhouseproject3@gmail.com" withHeight:303];
    [self.webView loadHTMLString:html baseURL:nil];
}


/*
 *  Allows links to open in Safari.
 */

- (BOOL)webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType
{
    if (inType == UIWebViewNavigationTypeLinkClicked)
    {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}

@end
