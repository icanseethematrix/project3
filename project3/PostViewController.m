//
//  PostViewController.m
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "PostViewController.h"
#import "Forum.h"

@implementation PostViewController

@synthesize identifier = _identifier;
@synthesize nameField = _nameField;
@synthesize postField = _postField;
@synthesize type = _type;


/*
 *  Loads the View.
 */

- (void)loadView
{
    [super loadView];
    
    // styles UITextView to look like input space
    self.postField.layer.borderWidth = 1.0f;
    self.postField.layer.borderColor = [[UIColor grayColor] CGColor];
    self.postField.layer.cornerRadius = 5;
    self.postField.clipsToBounds = YES;

    // brings up keyboard
    [self.nameField becomeFirstResponder];
}


/*
 *  Checks for errors and saves post.
 */

- (IBAction)submitPost:(id)sender;
{
    // limiting constants
    int maxNameLength = 100;
    int maxPostLength = 1000;
    
    // allow for customizable alert
    NSString *alertMessage = [[NSString alloc] init];
    
    // ensure forms are filled out
    if ([self.nameField.text isEqualToString:@""] || [self.postField.text isEqualToString:@""])
        alertMessage = [NSString stringWithFormat:@"You must enter both your name and a %@.", self.type];

    // ensure name not too long
    else if (self.nameField.text.length > maxNameLength)
        alertMessage = [NSString stringWithFormat:@"Name must be less than %d characters.", maxNameLength];
        
    // ensure post not too long
    else if (self.postField.text.length > maxPostLength)
        alertMessage = [NSString stringWithFormat:@"Post must be less than %d characters.", maxPostLength];

    // else submit post
    else
    {
        // save post
        Forum *forum = [[Forum alloc] init];
        [forum savePost:self.postField.text withName:self.nameField.text ofType:self.type andIdentifier:self.identifier];
        
        // return to previous view controller
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }

    // alert displayed if post not submitted
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fix your post!" 
                                                    message:alertMessage 
                                                   delegate:nil
                                          cancelButtonTitle:@"OK!"
                                          otherButtonTitles:nil];
    [alert show];
}


@end
