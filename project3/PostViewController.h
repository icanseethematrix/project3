//
//  PostViewController.h
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostViewController : UIViewController <UIAlertViewDelegate>

@property (nonatomic, readwrite, strong) NSString *identifier;
@property (nonatomic, readwrite, strong) IBOutlet UITextField *nameField;
@property (nonatomic, readwrite, strong) IBOutlet UITextView *postField;
@property (nonatomic, readwrite, strong) NSString *type;

- (IBAction)submitPost:(id)sender;

@end
