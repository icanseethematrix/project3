//
//  Calendar.m
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "Calendar.h"

@implementation Calendar


/*
 *  Gets calendar in agenda view for given email and sizes appropriately.
 */

- (NSString *)getCalendarHTMLForEmail:(NSString *)email withHeight:(int)height
{
    NSString *calendar = [NSString stringWithFormat:@"<html><iframe src=\"http://www.google.com/calendar/embed?showTitle=0&amp;showNav=0&amp;showTabs=0&amp;showPrint=0&amp;showCalendars=0&amp;mode=AGENDA&amp;height=600&amp;wkst=2&amp;hl=en_GB&amp;src=%@&amp;color=%23A32929&amp; style=\"border-width: 0pt;\" mce_style=\" border-width:0 \" frameborder=\"0\" height=\"%d\" width=\"300\"></iframe>", email, height];

    return calendar;
}


@end