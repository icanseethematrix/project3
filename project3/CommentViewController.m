//
//  CommentViewController.m
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "CommentViewController.h"
#import "Forum.h"
#import "PostViewController.h"

@implementation CommentViewController

@synthesize comments = _comments;
@synthesize question = _question;


/*
 *  Loads the view.  
 */

- (void)loadView
{
    [super loadView];
    
    // gets comments for given question
    Forum *forum = [[Forum alloc] init];
    self.comments = [[[forum getPostsOfType:@"comment" withIdentifier:self.question] objectAtIndex:0] objectForKey:@"comments"];    
}


/*
 *  Prepares for transition to PostViewController.
 */

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"commentToPost"])
    {
        PostViewController *postViewController = segue.destinationViewController;
        
        // initialize view with necessary data
        postViewController.type = @"comment";
        postViewController.identifier = self.question;
        postViewController.title = @"Post a comment:";
    }
}


/*
 *  Populates cells in table view.
 */

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // allows cells to be reused to save memory
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];

    // displays ith comment in ith cell
    cell.textLabel.text = [self.comments objectAtIndex:[indexPath row]];

    // allows text to wrap across lines
    cell.textLabel.numberOfLines = 10;
    cell.textLabel.lineBreakMode = UILineBreakModeTailTruncation;

    return cell;
}


/*
 *  Determines number of rows in table view section.
 */

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.comments count];
}


/*
 *  Refreshes view when called via "pop" from navigation controller.
 */

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadView];
}


@end
