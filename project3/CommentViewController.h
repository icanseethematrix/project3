//
//  CommentViewController.h
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, readwrite, strong) NSArray *comments;
@property (nonatomic, readwrite, strong) NSString *question;

@end
