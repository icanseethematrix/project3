//
//  AppDelegate.m
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

@synthesize window = _window;


/*
 *  Customize application after launch.
 */

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // connect to Parse database
    [Parse setApplicationId:@"EoLgLj5qtZCqzcyH2be3HCnb76AgSEMj8SzDaHWr" 
                  clientKey:@"0X645fJjV2RV1zKYpXyqTc5KExNNSjBjK4XM9pmn"];

    [window makeKeyAndVisible];
    return YES;
}


@end
