//
//  AboutHouseViewController.m
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "AboutHouseViewController.h"
#import "MapAnnotation.h"

@implementation AboutHouseViewController

@synthesize mapView = _mapView;
@synthesize scrollView = _scrollView;


/*
 *  Loads the view.
 */

- (void)loadView
{
    [super loadView];
    
    // make scroll view correct size
    self.scrollView.contentSize = CGSizeMake(320,720);

    
    // initialize map view
    self.mapView.mapType = MKMapTypeHybrid;
    
    
    // center map on Dudley House
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = 42.373597;
    coordinate.longitude = -71.118536;
    
    // zoom to a reasonable degree
    MKCoordinateSpan span;
    span.latitudeDelta = 0.005;
    span.longitudeDelta = 0.005;

    MKCoordinateRegion region = {coordinate, span};
    [self.mapView setRegion:region];
    
    // drop pin on house
    NSString *title = @"Dudley House";
    MapAnnotation *dudley = [[MapAnnotation alloc] initWithCoordinate:coordinate title:title];    
	[self.mapView addAnnotation:dudley];
}


/*
 *  Called when annotations have been added to map.
 */

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views 
{    
    // select annotation and display flag
    [self.mapView selectAnnotation:[self.mapView.annotations objectAtIndex:0] animated:YES];
}


@end
