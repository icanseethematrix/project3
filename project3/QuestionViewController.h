//
//  QuestionViewController.h
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong, readwrite) NSString *discussion;
@property (nonatomic, strong, readwrite) NSArray *questions;

@end
