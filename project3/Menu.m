//
//  Menu.m
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "Menu.h"

@implementation Menu


/*
 *  Gets menu with designated name for UIWebView.
 */

- (NSURLRequest *)getMenuWithName:(NSString *)name
{
    NSString *urlAddress = [[NSBundle mainBundle] pathForResource:name ofType:@"pdf"];
    NSURL *url = [NSURL fileURLWithPath:urlAddress];
    
    return [NSURLRequest requestWithURL:url];
}

@end
