//
//  QuestionViewController.m
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "CommentViewController.h"
#import "Forum.h"
#import "PostViewController.h"
#import "QuestionViewController.h"

@implementation QuestionViewController

@synthesize discussion = _discussion;
@synthesize questions = _questions;


/*
 *  Loads the view.
 */

- (void)loadView
{
    [super loadView];
    
    // gets questions for given discussion
    Forum *forum = [[Forum alloc] init];
    self.questions = [forum getPostsOfType:@"question" withIdentifier:self.discussion];    
}


/*
 *  Prepares for transition to CommentViewController or PostViewController.
 */

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"questionToComment"])
    {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]];
        CommentViewController *commentViewController = segue.destinationViewController;
        
        // initialize view with necessary data
        commentViewController.question = cell.textLabel.text;
        commentViewController.title = @"Comments";
    }
    
    if ([segue.identifier isEqualToString:@"questionToPost"])
    {
        PostViewController *postViewController = segue.destinationViewController;
        
        // initialize view with necessary data
        postViewController.type = @"question";
        postViewController.identifier = self.discussion;
        postViewController.title = @"Post a question:";
    }
}


/*
 *  Populates cells in table view.
 */

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // allows cells to be reused to save memory
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    // displays ith question in ith cell
    cell.textLabel.text = [NSString stringWithFormat:@"%@", [[self.questions objectAtIndex:[indexPath row]] objectForKey:@"question"]];
    
    // allows text to wrap across lines
    cell.textLabel.numberOfLines = 10;
    cell.textLabel.lineBreakMode = UILineBreakModeTailTruncation;

    return cell;
}


/*
 *  Determines number of rows in table view section.
 */

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.questions count];
}


/*
 *  Refreshes view when called via "pop" from navigation controller.
 */

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadView];
}


@end
