//
//  Forum.h
//  project3
//
//  Created by Joseph Botros on 4/30/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface Forum : NSObject

- (NSArray *)getPostsOfType:(NSString *)type withIdentifier:(NSString *)identifier;
- (void)savePost:(NSString *)post withName:(NSString *)name ofType:(NSString *)type andIdentifier:(NSString *)identifier;

@end
