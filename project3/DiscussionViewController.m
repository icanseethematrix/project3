//
//  DiscussionViewController.m
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "DiscussionViewController.h"
#import "QuestionViewController.h"
#import "Forum.h"

@implementation DiscussionViewController

@synthesize tblSimpleTable = _tblSimpleTable;
@synthesize discussions = _discussions;


/*
 *  Loads the view.
 */

- (void)loadView
{
    [super loadView];
    
    // gets discussions to display
    Forum *forum = [[Forum alloc] init];
    self.discussions = [forum getPostsOfType:@"discussion" withIdentifier:nil];
}


/*
 *  Prepares for transition to QuestionViewController.
 */

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    QuestionViewController *questionViewController = segue.destinationViewController;
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]];
    questionViewController.discussion = cell.textLabel.text;
    questionViewController.title = @"Questions";
}


/*
 *  Populates cells in table view.
 */

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // allows cells to be reused to save memory
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    

    // displays ith discussion in ith cell
    cell.textLabel.text = [[self.discussions objectAtIndex:[indexPath row]] objectForKey:@"discussion"];    

    // allows text to wrap across lines
    cell.textLabel.numberOfLines = 10;
    cell.textLabel.lineBreakMode = UILineBreakModeTailTruncation;

    return cell;
}


/*
 *  Determines number of rows in table view section.
 */

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.discussions count];
}


@end