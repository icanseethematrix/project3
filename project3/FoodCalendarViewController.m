//
//  FoodCalendarViewController.m
//  project3
//
//  Created by Joseph Botros on 4/20/12.
//  Copyright (c) 2012 Harvard University. All rights reserved.
//

#import "FoodCalendarViewController.h"
#import "Calendar.h"

@implementation FoodCalendarViewController

@synthesize webView = _webView;


/*
 *  Loads the view.
 */

- (void)loadView
{
    [super loadView];
    
    // gets calendar for the cafe
    Calendar *calendar = [[Calendar alloc] init];
    NSString *html = [calendar getCalendarHTMLForEmail:@"cafegatorojo@gmail.com" withHeight:370];
    [self.webView loadHTMLString:html baseURL:nil];
}


/*
 *  Allows links to open in Safari.
 */

- (BOOL)webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType
{
    if (inType == UIWebViewNavigationTypeLinkClicked)
    {
        [[UIApplication sharedApplication] openURL:[inRequest URL]];
        return NO;
    }
    
    return YES;
}

@end
